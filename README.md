# Icecast Docker Images
This repository contains the Dockerfiles for Docker images used in Icecast
related projects for their CI.

It does not provide a Docker image for the Icecast Server software!

## Images

### Debian 6 (Squeeze)
This is a very old Debian version, the primary intention of this image is to check
compatibility with very old GCC and Autotools versions.

Installed tools:
- gcc (`gcc (Debian 4.4.5-8) 4.4.5`)
- clang (`clang version 1.1 (Debian 2.7-3)`, `llvm-2.7`)
- m4 (`m4 (GNU M4) 1.4.14`)
- automake (`automake (GNU automake) 1.11.1`)
- autoconf (`autoconf (GNU Autoconf) 2.67`)
- libtool (`ltmain.sh (GNU libtool) 2.2.6b`)
- git (`git version 1.7.2.5`)

## Adding Images
To  add a new image:

1. Create a new folder with the name of the image (it may not contain spaces or other URL-unsafe charaters):

    ```
    mkdir alpine-example
    ```

2.  Create a Dockerfile in that folder

    ```
    vim alpine-example/Dockerfile
    ```

3. Add the build job for the image to the [.gitlab-ci.yml](.gitlab-ci.yml):

    ```yaml
    Build (alpine-example):
      extends: .image-build-common
      variables:
        IMAGE_NAME: alpine-example
    ```

    Note that the folder name has to match the `IMAGE_NAME`.

## Build Images manually

You need the tools "yq" and "jq":

```
yq read -j .gitlab-ci.yml  | jq 'to_entries | .[] | select (.key | test("^Build")) | (.key + " docker build -t " + .value.variables.IMAGE_NAME + " --build-arg BASE_IMAGE=\"" + .value.variables.BASE_IMAGE + "\"  --build-arg PKGS=\"" + .value.variables.PKGS + "\" -f " + .value.variables.IMAGE_DIR + "/Dockerfile " + .value.variables.IMAGE_DIR ) ' -r
```
